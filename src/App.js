import React, { Component } from 'react';

import Provider from './Provider';
import ToDoComponent from './ToDoComponent'

import { createStore, reducer } from './slomux';

class App extends Component {
  render() {
    return (
      <Provider store={createStore(reducer, [])}>
        <ToDoComponent title="Список задач"/>
      </Provider>
    );
  }
}

export default App;
