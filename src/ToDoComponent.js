import React from 'react';

import { connect, addTodo } from './slomux';

class ToDoComponent extends React.Component {
  state = {
    todoText: ''
  };

  render() {
    console.log(this.props);
    const { todos = [], title } = this.props;
    return (
      <div>
        <label>{title || 'Без названия'}</label>
        <div>
          <input
            value={this.state.todoText}
            placeholder="Название задачи"
            onChange={e => this.setState({todoText: e.target.value})}
          />
          <button onClick={this.addTodo}>Добавить</button>
          <ul>
            {todos.map && todos.map(todo => <li>{todo}</li>)}
          </ul>
        </div>
      </div>
    )
  }

  addTodo = () => {
    this.props.addTodo(this.state.todoText);
    this.setState({todoText: ''});
  }
}

export default connect(todos => ({
  todos,
}), dispatch => ({
  addTodo: text => dispatch(addTodo(text)),
}))(ToDoComponent)
