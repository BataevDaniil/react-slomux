import React from 'react';

export const createStore = (reducer, initialState) => {
  let currentState = Object.assign({}, initialState);
  const listeners = [];

  const getState = () => currentState;
  const dispatch = action => {
    console.log(action);
    currentState = reducer(currentState, action);
    listeners.forEach(listener => listener())
  };

  const subscribe = listener => listeners.push(listener);

  return {getState, dispatch, subscribe}
};

export const connect = (mapStateToProps, mapDispatchToProps) =>
  Component => {
    return class extends React.Component {
      render() {
        return (
          <Component
            {...mapStateToProps(window.store.getState())}
            {...this.props}
            {...mapDispatchToProps(window.store.dispatch)}
          />
        )
      }

      componentDidMount() {
        window.store.subscribe(this.handleChange)
      }

      handleChange = () => {
        this.forceUpdate()
      }
    }
  };


// actions
export const ADD_TODO = 'ADD_TODO';

// action creators
export const addTodo = todo => ({
  type: ADD_TODO,
  payload: todo,
});

// reducers
export const reducer = (state = [], action) => {
  switch (action.type) {
    case ADD_TODO:
      return [...state, action.payload];
    default:
      return state
  }
};

